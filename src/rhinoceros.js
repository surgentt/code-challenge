const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');
const speciesOptions = [
  'white_rhinoceros', 'black_rhinoceros', 'indian_rhinoceros', 
  'javan_rhinoceros', 'sumatran_rhinoceros'
];

exports.getAll = () => {
  return rhinoceroses;
};

exports.newRhinoceros = (data) => {
  newId = Object.keys(rhinoceroses).length+1;
  const newRhino = { 
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses[newId]=newRhino;
  return {[newId]: newRhino};
};

exports.getId = (id) => {
  return {[id]: rhinoceroses[id]}
}

exports.findAllBySpecies = (species_selection) => {
  let rhinoceroses_reponse = findAllBy('species', species_selection)
  return rhinoceroses_reponse;
}

exports.findAllByName = (name) => {
  let rhinoceroses_reponse = findAllBy('name', name)
  return rhinoceroses_reponse;
}

exports.endangered = () => {
  let rhinoceroses_reponse = {}
  for(const speciesOption of speciesOptions){
    let groupedRhinos = findAllBy('species', speciesOption);
    if(Object.keys(groupedRhinos).length <= 2){
      Object.assign(rhinoceroses_reponse, groupedRhinos);
    }
  }
  return rhinoceroses_reponse;
}

function findAllBy(attribute, selection){
  let rhinoceroses_reponse = {}
  const keys = Object.keys(rhinoceroses);
  for (const key of keys) {
    if(rhinoceroses[key][attribute] === selection){
      rhinoceroses_reponse[key] = rhinoceroses[key]
    }
  }  
  return rhinoceroses_reponse
}
