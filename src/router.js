const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

router.get('/rhinoceros', (ctx, next) => {
  const rhinoceroses = model.getAll();
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/:id', (ctx, next) => {
  const rhinoceroses = model.getId(ctx.params.id);
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/species/:type', (ctx, next) => {
  const rhinoceroses = model.findAllBySpecies(ctx.params.type);
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/names/:name', (ctx, next) => {
  const rhinoceroses = model.findAllByName(ctx.params.name);
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/endangered/all', (ctx, next) => {
  const rhinoceroses = model.endangered();
  ctx.response.body = { rhinoceroses };
});

router.post('/rhinoceros', (ctx, next) => {
  let expectedResponse = validations(ctx.request.body, ctx);
  if(expectedResponse['code']===201){
    ctx.response.body = model.newRhinoceros(ctx.request.body);
  } else {
    ctx.response.body = {errors: expectedResponse['errors']};
    ctx.status = expectedResponse['code'];
  }
});

function validations(rhinoceros, ctx) {
  let expectedResponse = {code: 201, errors: {}};
  if (rhinoceros.name.length <= 1 || rhinoceros.name.length >= 20){
    expectedResponse['code'] = 422;
    expectedResponse['errors']['name'] = 'Length Needs to be between 1 and 20 character';
  }
  const valid_species = [
    'white_rhinoceros', 'black_rhinoceros', 'indian_rhinoceros', 
    'javan_rhinoceros', 'sumatran_rhinoceros'
  ];
  if(!valid_species.includes(rhinoceros.species)){
    expectedResponse['code'] = 422;
    expectedResponse['errors']['species'] = 'species is not valid';
  }
  if(Object.keys(rhinoceros).sort().toString() !== 'name,species' ){
    expectedResponse['code'] = 403;
    expectedResponse['errors']['forbidden_attribute'] = 'Additonal Keys Should not be passed in body';
  }
  return expectedResponse
}

module.exports = router;